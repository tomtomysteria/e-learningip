<?php

namespace IP\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="IP\CoreBundle\Entity\Tuto", inversedBy="tags", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $tuto;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Tag
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set tuto
     *
     * @param \IP\CoreBundle\Entity\Tuto $tuto
     * @return Tag
     */
    public function setTuto(\IP\CoreBundle\Entity\Tuto $tuto)
    {
        $this->tuto = $tuto;

        return $this;
    }

    /**
     * Get tuto
     *
     * @return \IP\CoreBundle\Entity\Tuto 
     */
    public function getTuto()
    {
        return $this->tuto;
    }
}
