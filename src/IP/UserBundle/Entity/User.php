<?php
namespace IP\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @Assert\NotNull()
     */
    protected $email;
    
    /**
     * @Assert\NotNull()
     */
    protected $username;

    /**
     * @ORM\OneToMany(targetEntity="IP\CoreBundle\Entity\Note", mappedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $note;

    /**
     * @ORM\OneToMAny(targetEntity="IP\CoreBundle\Entity\Tuto", mappedBy="user", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $tuto;
    
    public function __construct()
    {
        parent::__construct();
        // your own logic
//        $this->roles = array('ROLE_ADMIN');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Add note
     *
     * @param \IP\CoreBundle\Entity\Note $note
     * @return User
     */
    public function addNote(\IP\CoreBundle\Entity\Note $note)
    {
        $this->note[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \IP\CoreBundle\Entity\Note $note
     */
    public function removeNote(\IP\CoreBundle\Entity\Note $note)
    {
        $this->note->removeElement($note);
    }

    /**
     * Get note
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Add tuto
     *
     * @param \IP\CoreBundle\Entity\Tuto $tuto
     * @return User
     */
    public function addTuto(\IP\CoreBundle\Entity\Tuto $tuto)
    {
        $this->tuto[] = $tuto;

        return $this;
    }

    /**
     * Remove tuto
     *
     * @param \IP\CoreBundle\Entity\Tuto $tuto
     */
    public function removeTuto(\IP\CoreBundle\Entity\Tuto $tuto)
    {
        $this->tuto->removeElement($tuto);
    }

    /**
     * Get tuto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTuto()
    {
        return $this->tuto;
    }
}
