<?php

namespace IP\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use IP\CoreBundle\Entity\Type;
use IP\CoreBundle\Form\TypeType;

/**
 * @Route("/admin/type")
 */
class TypeController extends Controller
{

    /**
     * @Route("/")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPCoreBundle:Type')->findAll();

        $deleteForm = $this->createDeleteForm();

        return array(
            'entities' => $entities,
            'deleteForm' => $deleteForm->createView(),
        );
    }

    /**
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $entity = new Type();
        $form = $this->createForm(new TypeType(), $entity, array('method' => 'POST'));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array('class' => 'btn btn-primary')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', "Le type a été créé avec succès.");
            return $this->redirect($this->generateUrl('ip_admin_type_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", requirements={"id" = "\d+"})
     * @Method({"GET", "PUT"})
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPCoreBundle:Type')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $form = $this->createForm(new TypeType(), $entity, array('method' => 'PUT'));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array('class' => 'btn btn-primary')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', "Le type a été modifié avec succès.");
            return $this->redirect($this->generateUrl('ip_admin_type_index'));
        }

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/{id}", requirements={"id" = "\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPCoreBundle:Type')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        return array(
            'entity'      => $entity,
        );
    }

    /**
     * @Route("/delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $form = $this->createDeleteForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $deleteFormRequest = $request->request->get('form');
            if (isset($deleteFormRequest['tags'])) {
                $entities2Delete = $deleteFormRequest['tags'];
                foreach ($entities2Delete as $key => $entity) {
                    $em = $this->getDoctrine()->getManager();
                    $entity = $em->getRepository('IPCoreBundle:Type')->find($key);
                    if (!$entity) {
                        throw $this->createNotFoundException('Unable to find entity.');
                    }
                    $em->remove($entity);
                }
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('ip_admin_type_index'));
    }

    private function createDeleteForm()
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ip_admin_type_delete'))
            ->setMethod('DELETE')
            ->add('tags', 'collection', array(
                'type' => 'hidden',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Supprimer',
                'attr' => array('class' => 'hide'),
            ))
            ->getForm()
            ;
    }

}
