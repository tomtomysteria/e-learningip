$(function(){
    function addTagDeleteForm($collectionHolder, id2Delete) {
        // Récupère l'élément ayant l'attribut data-prototype
        var prototype = $collectionHolder.attr('data-prototype');
        // Remplace '__name__' dans le HTML du prototype par l'id de l'item sélectionné
        var newForm = prototype.replace(/__name__/g, id2Delete);
        $('#formMultiDelete #form #formDeleteTags').prepend(newForm);
    }    
    
    function toDeleteIsChecked($toDelete) {
        var isChecked = false;
        $toDelete.each(function(){
            if ($(this).is(':checked')) {
                isChecked = true;
                return false;
            }
        }); 
        return isChecked;
    }
    
    var $collectionHolder = $('#formMultiDelete #form_tags');
    var $toDelete = $('input[type="checkbox"].toDelete');
    $('#formMultiDelete #form').prepend('<div id="formDeleteTags"></div>');
    
    $toDelete.on('change', function(e){
        e.preventDefault();
        if ($(this).is(':checked')) {
            addTagDeleteForm($collectionHolder, $(this).val());
        } else {
            $('#formMultiDelete #form #formDeleteTags input[id="form_tags_' + $(this).val() + '"]').remove();
        }        
    });
    
    $('#multiDeleteAction').on('click', function(e){
        e.preventDefault();
        
        if (!toDeleteIsChecked($toDelete)) {
            alert('Vous devez sélectionner au moins un élément');
        } else {
            if (confirm('Êtes-vous sûr de vouloir effectuer cette suppression ?')) {
               $('#formMultiDelete').submit(); 
            }
        }
    });
    
    $('.deleteAll').on('change', function(e){
        e.preventDefault();
        $('#formMultiDelete #form #formDeleteTags input[type="hidden"]').remove();
        if ($(this).is(':checked')) {
            $toDelete.each(function(){
                $(this)[0].checked = true;
                addTagDeleteForm($collectionHolder, $(this).val());
            });
        } else {
            $toDelete.each(function(){
                $(this)[0].checked = false;
                $('#formMultiDelete #form #formDeleteTags input[id="form_tags_' + $(this).val() + '"]').remove();
            });            
        }         
    });
});