<?php

namespace IP\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TutoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lienPost', 'url')
            ->add('descriptionPost')
            ->add('categorie', 'entity', array(
                'empty_value' => 'Choisissez une categorie',
                'class' => 'IPCoreBundle:Categorie'
            ))
            ->add('tags', 'collection', array(
                'type' => new TagType(),
                'allow_add' => true,
                'allow_delete' => true
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IP\CoreBundle\Entity\Tuto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ip_corebundle_tuto';
    }
}
