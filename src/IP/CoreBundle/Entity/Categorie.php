<?php

namespace IP\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Categorie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="IP\CoreBundle\Entity\Tuto", mappedBy="tuto", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $tuto;




    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return Categorie
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Categorie
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tuto = new \Doctrine\Common\Collections\ArrayCollection();
    }


    public function __toString()
    {
        return $this->getLibelle();
    }

    /**
     * Add tuto
     *
     * @param \IP\CoreBundle\Entity\Tuto $tuto
     * @return Categorie
     */
    public function addTuto(\IP\CoreBundle\Entity\Tuto $tuto)
    {
        $this->tuto[] = $tuto;

        return $this;
    }

    /**
     * Remove tuto
     *
     * @param \IP\CoreBundle\Entity\Tuto $tuto
     */
    public function removeTuto(\IP\CoreBundle\Entity\Tuto $tuto)
    {
        $this->tuto->removeElement($tuto);
    }

    /**
     * Get tuto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTuto()
    {
        return $this->tuto;
    }
}
