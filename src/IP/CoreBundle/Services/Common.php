<?php

namespace IP\CoreBundle\Services;

class Common
{
    /**
    * Function : generateToken
    * Génère un texte d'un nombre de caractères, $nbr à partir d'une chaîne de caractères, $pattern
    */	    
    public function generateToken($nbr, $pattern = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    {
        $string="";
        srand((double)microtime()*1000000);
        for($i=0; $i<$nbr; $i++){
                $string.=$pattern[rand()%strlen($pattern)];		
        }
        return $string;
    } 
    
    
    /**
    * Function : limitText
    *
    * Limite le texte, $text au nombre de caractères, $nbr
    */	
    public function limitText($text, $nbr){
        $subtext=substr($text, 0, $nbr);
        return(strlen($text)>$nbr ? substr($subtext, 0, strrpos($subtext, " "))."..." : $text); 
    }    
}