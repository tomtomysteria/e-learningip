<?php

namespace IP\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use IP\CoreBundle\Form\TutoType;

class AdminTutoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isValidePost')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IP\CoreBundle\Entity\Tuto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ip_corebundle_adminTuto';
    }

    public function getParent()
    {
        return new TutoType();
    }
}
