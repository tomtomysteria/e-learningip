<?php
# src/IP/CoreBundle/Form/NotMapped/ContactType.php

namespace IP\CoreBundle\Form\NotMapped;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('nom', 'text', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Nom',
                    )
                ))
                ->add('prenom', 'text', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Prénom',
                    )
                ))
                ->add('telephone', 'text', array(
                    'label' => false,
                    'required' => false,
                    'attr' => array(
                        'placeholder' => 'Téléphone',
                    )                    
                ))
                ->add('email', 'email', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Email',
                    )                    
                ))
                ->add('message', 'textarea', array(
                    'label' => false,
                    'attr' => array(
                        'placeholder' => 'Votre demande',
                    )                    
                ))
	;
    }

    public function getName()
    {
        return 'ip_core_notmapped_contact';
    }
    
}
