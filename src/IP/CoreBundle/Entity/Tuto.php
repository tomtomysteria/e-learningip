<?php

namespace IP\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tuto
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tuto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LienPost", type="text")
     */
    private $lienPost;

    /**
     * @var string
     *
     * @ORM\Column(name="DescriptionPost", type="text")
     */
    private $descriptionPost;

    /**
     * @var boolean
     *
     * @ORM\Column(name="IsValidePost", type="boolean", nullable=true)
     */
    private $isValidePost;

    /**
     * @ORM\OneToMany(targetEntity="IP\CoreBundle\Entity\Tag", mappedBy="tuto", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="IP\CoreBundle\Entity\Categorie", inversedBy="tuto", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $categorie;

    /**
     * @ORM\ManyToMany(targetEntity="IP\CoreBundle\Entity\Type", mappedBy="tuto", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="IP\CoreBundle\Entity\Note", mappedBy="tuto", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity="IP\UserBundle\Entity\User", inversedBy="tuto", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lienPost
     *
     * @param string $lienPost
     * @return Tuto
     */
    public function setLienPost($lienPost)
    {
        $this->lienPost = $lienPost;

        return $this;
    }

    /**
     * Get lienPost
     *
     * @return string 
     */
    public function getLienPost()
    {
        return $this->lienPost;
    }

    /**
     * Set descriptionPost
     *
     * @param string $descriptionPost
     * @return Tuto
     */
    public function setDescriptionPost($descriptionPost)
    {
        $this->descriptionPost = $descriptionPost;

        return $this;
    }

    /**
     * Get descriptionPost
     *
     * @return string 
     */
    public function getDescriptionPost()
    {
        return $this->descriptionPost;
    }

    /**
     * Set isValidePost
     *
     * @param boolean $isValidePost
     * @return Tuto
     */
    public function setIsValidePost($isValidePost)
    {
        $this->isValidePost = $isValidePost;

        return $this;
    }

    /**
     * Get isValidePost
     *
     * @return boolean 
     */
    public function getIsValidePost()
    {
        return $this->isValidePost;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->categorie = new \Doctrine\Common\Collections\ArrayCollection();
        $this->type = new \Doctrine\Common\Collections\ArrayCollection();
        $this->note = new \Doctrine\Common\Collections\ArrayCollection();

        $this->date = new \DateTime();
    }

    /**
     * Add tags
     *
     * @param \IP\CoreBundle\Entity\Tag $tags
     * @return Tuto
     */
    public function addTag(\IP\CoreBundle\Entity\Tag $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \IP\CoreBundle\Entity\Tag $tags
     */
    public function removeTag(\IP\CoreBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }


    /**
     * Add type
     *
     * @param \IP\CoreBundle\Entity\Type $type
     * @return Tuto
     */
    public function addType(\IP\CoreBundle\Entity\Type $type)
    {
        $this->type[] = $type;

        return $this;
    }

    /**
     * Remove type
     *
     * @param \IP\CoreBundle\Entity\Type $type
     */
    public function removeType(\IP\CoreBundle\Entity\Type $type)
    {
        $this->type->removeElement($type);
    }

    /**
     * Get type
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add note
     *
     * @param \IP\CoreBundle\Entity\Note $note
     * @return Tuto
     */
    public function addNote(\IP\CoreBundle\Entity\Note $note)
    {
        $this->note[] = $note;

        return $this;
    }

    /**
     * Remove note
     *
     * @param \IP\CoreBundle\Entity\Note $note
     */
    public function removeNote(\IP\CoreBundle\Entity\Note $note)
    {
        $this->note->removeElement($note);
    }

    /**
     * Get note
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set categorie
     *
     * @param \IP\CoreBundle\Entity\Categorie $categorie
     * @return Tuto
     */
    public function setCategorie(\IP\CoreBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \IP\CoreBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set user
     *
     * @param \IP\UserBundle\Entity\User $user
     * @return Tuto
     */
    public function setUser(\IP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \IP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Tuto
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
}
