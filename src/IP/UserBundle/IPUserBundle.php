<?php

namespace IP\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IPUserBundle extends Bundle
{
  public function getParent()
  {
    return 'FOSUserBundle';
  }    
}