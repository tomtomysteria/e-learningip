$(function(){
    $('.goBack').on('click', function(e){
        e.preventDefault();
        window.history.back();
    });
    
    //$('input[type="checkbox"]:not(.toDelete, .deleteAll)').bootstrapSwitch();
    
//    $('[type="radio"]').bootstrapSwitch();
    
    
    $('.sidebar-toggle-box').on('click', function(e){
        e.preventDefault();
        // Si la sidebar est ouverte, alors on la ferme
        if ($('#sidebar').css('margin-left') === '0px') {
            $("#sidebar").animate({
                marginLeft: '-240px'
            }, 0);   
            $("#main-content").animate({
                marginLeft: '0'
            }, 0);     
        // Si la sidebar est fermée, alors on l'ouvre    
        } else if ($('#sidebar').css('margin-left') === '-240px') {
            $("#sidebar").animate({
                marginLeft: '0'
            }, 0); 
            $("#main-content").animate({
                marginLeft: '240px'
            }, 0);              
        }
    });
    

    
});