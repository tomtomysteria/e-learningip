<?php

namespace IP\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Type
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="LibelleType", type="string", length=255)
     */
    private $libelleType;

    /**
     * @ORM\ManyToMany(targetEntity="IP\CoreBundle\Entity\Tuto", inversedBy="tuto", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $tuto;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelleType
     *
     * @param string $libelleType
     * @return Type
     */
    public function setLibelleType($libelleType)
    {
        $this->libelleType = $libelleType;

        return $this;
    }

    /**
     * Get libelleType
     *
     * @return string 
     */
    public function getLibelleType()
    {
        return $this->libelleType;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tuto = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add tuto
     *
     * @param \IP\CoreBundle\Entity\Tuto $tuto
     * @return Type
     */
    public function addTuto(\IP\CoreBundle\Entity\Tuto $tuto)
    {
        $this->tuto[] = $tuto;

        return $this;
    }

    /**
     * Remove tuto
     *
     * @param \IP\CoreBundle\Entity\Tuto $tuto
     */
    public function removeTuto(\IP\CoreBundle\Entity\Tuto $tuto)
    {
        $this->tuto->removeElement($tuto);
    }

    /**
     * Get tuto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTuto()
    {
        return $this->tuto;
    }
}
