<?php

namespace IP\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IP\CoreBundle\Entity\Categorie;
use IP\AdminBundle\Form\CategorieType;

/**
 * Categorie controller.
 *
 * @Route("/admin/categorie")
 */
class CategorieController extends Controller
{

    /**
     * Lists all Categorie entities.
     *
     * @Route("/")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPCoreBundle:Categorie')->findAll();

        $deleteForm = $this->createDeleteForm();

        return array(
            'entities' => $entities,
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Creates a new Categorie entity.
     *
     * @Route("/", name="categorie_create")
     * @Method("POST")
     * @Template("IPCoreBundle:Categorie:new.html.twig")
     */
    public function createAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }       

        $entity = new Categorie();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($entity);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', "La catégorie a été créée avec succès.");
            return $this->redirect($this->generateUrl('ip_admin_categorie_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Categorie entity.
     *
     * @param Categorie $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Categorie $entity)
    {
        $form = $this->createForm(new CategorieType(), $entity, array(
            'action' => $this->generateUrl('categorie_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Categorie entity.
     *
     * @Route("/new", name="categorie_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }       

        $entity = new Categorie();
        $form   = $this->createCreateForm($entity);

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array('class' => 'btn btn-primary')));

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Categorie entity.
     *
     * @Route("/{id}", name="categorie_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }       

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPCoreBundle:Categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Categorie entity.
     *
     * @Route("/{id}/edit", name="categorie_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }       

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPCoreBundle:Categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }

        $form = $this->createEditForm($entity);
        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array('class' => 'btn btn-primary')));

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to edit a Categorie entity.
    *
    * @param Categorie $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Categorie $entity)
    {
        $form = $this->createForm(new CategorieType(), $entity, array(
            'action' => $this->generateUrl('categorie_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Categorie entity.
     *
     * @Route("/{id}", name="categorie_update")
     * @Method("PUT")
     * @Template("IPCoreBundle:Categorie:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }       

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPCoreBundle:Categorie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', "La catégorie a été modifiée avec succès.");
            return $this->redirect($this->generateUrl('ip_admin_categorie_index'));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Categorie entity.
     *
     * @Route("/delete", name="categorie_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }       
        
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }        
        
        $form = $this->createDeleteForm();
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $deleteFormRequest = $request->request->get('form');
            if (isset($deleteFormRequest['tags'])) {
                $entities2Delete = $deleteFormRequest['tags'];
                foreach ($entities2Delete as $key => $entity) {
                    $em = $this->getDoctrine()->getManager();
                    $entity = $em->getRepository('IPCoreBundle:Categorie')->find($key);
                    if (!$entity) {
                        throw $this->createNotFoundException('Unable to find Categorie entity.');
                    }
                    $em->remove($entity);                
                }   
                $em->flush();
            }
        }          

        return $this->redirect($this->generateUrl('ip_admin_categorie_index'));
    }

    /**
     * Creates a form to delete a Categorie entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm()
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorie_delete'))    
            ->setMethod('DELETE')
            ->add('tags', 'collection', array(
                'type' => 'hidden',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ))      
            ->add('submit', 'submit', array(
                'label' => 'Supprimer',
                'attr' => array('class' => 'hide'),
                ))
            ->getForm()
        ; 
    }  
}
