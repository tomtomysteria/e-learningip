<?php

namespace IP\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="IP\CoreBundle\Entity\ImageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Image
{

  /**
   * @var integer
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="extension", type="string", length=255)
   */
  private $extension;

  /**
   * @var string
   *
   * @ORM\Column(name="alt", type="string", length=255)
   */
  private $alt;
  
  /**
   * @var string
   * 
   * @ORM\Column(name="upload_dir", type="text")
   */
  private $uploadDir;  
  
  /**
   * @var DateTime
   * 
   * @ORM\Column(name="date_creation", type="datetime")
   */
  private $dateCreation;
  
  /**
   *
   * 
   * @Assert\File(
   *    mimeTypes = {"image/jpeg", "image/png"},
   *    mimeTypesMessage = "Only the filetypes JPG and PNG are allowed."
   * )
   *  
   */
  private $file;
  // On ajoute cet attribut pour y stocker temporairement le nom du fichier
  private $tempFilename;
  

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set extension
   *
   * @param string $extension
   * @return Image
   */
  public function setExtension($extension)
  {
    $this->extension = $extension;

    return $this;
  }

  /**
   * Get extension
   *
   * @return string 
   */
  public function getExtension()
  {
    return $this->extension;
  }

  /**
   * Set alt
   *
   * @param string $alt
   * @return Image
   */
  public function setAlt($alt)
  {
    $this->alt = $alt;

    return $this;
  }

  /**
   * Get alt
   *
   * @return string 
   */
  public function getAlt()
  {
    return $this->alt;
  }

  public function getFile()
  {
    return $this->file;
  }

  // On modifie le setter de File, pour prendre en compte l'upload d'un fichier lorsqu'il en existe déjà un autre
  public function setFile(UploadedFile $file)
  {
    $this->file = $file;

    // On vérifie si on avait déjà un fichier pour cette entité
    if (null !== $this->extension) {
      // On sauvegarde l'extension du fichier pour le supprimer plus tard
      $this->tempFilename = $this->extension;

      // On réinitialise les valeurs des attributs extension et alt
      $this->extension = null;
      $this->alt = null;
    }
  }

  /**
   * @ORM\PrePersist()
   * @ORM\PreUpdate()
   */
  public function preUpload()
  {
    // Si jamais il n'y a pas de fichier (champ facultatif)
    if (null === $this->file) {
      return;
    }

    // Le nom du fichier est son id, on doit juste stocker également son extension
    $this->extension = $this->file->guessExtension();

    // Et on génère l'attribut alt de la balise <img>, à la valeur du nom du fichier sur le PC de l'internaute
    $this->alt = $this->file->getClientOriginalName();
  }

  /**
   * @ORM\PostPersist()
   * @ORM\PostUpdate()
   */
  public function upload()
  {
    // Si jamais il n'y a pas de fichier (champ facultatif)
    if (null === $this->file) {
      return;
    }

    // Si on avait un ancien fichier, on le supprime
    if (null !== $this->tempFilename) {
      $oldFile = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->tempFilename;
      if (file_exists($oldFile)) {
        unlink($oldFile);
      }
    }

    // On déplace le fichier envoyé dans le répertoire de notre choix
    $this->file->move(
            $this->getUploadRootDir(), // Le répertoire de destination
            $this->id . '.' . $this->extension   // Le nom du fichier à créer, ici « id.extension »
    );
  }

  /**
   * @ORM\PreRemove()
   */
  public function preRemoveUpload()
  {
    // On sauvegarde temporairement le nom du fichier, car il dépend de l'id
    $this->tempFilename = $this->getUploadRootDir() . '/' . $this->id . '.' . $this->extension;
  }

  /**
   * @ORM\PostRemove()
   */
  public function removeUpload()
  {
    // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
    if (file_exists($this->tempFilename)) {
      // On supprime le fichier
      unlink($this->tempFilename);
    }
  }
  
  public function setUploadDir($uploadDir)
  {
    $this->uploadDir = $uploadDir;
    return $this;
  }

  public function getUploadDir()
  {
    // On retourne le chemin relatif vers l'image pour un navigateur
//    return 'uploads/img';
    
    return $this->uploadDir;
  }

  protected function getUploadRootDir()
  {
    // On retourne le chemin relatif vers l'image pour notre code PHP
    return __DIR__ . '/../../../../web/' . $this->getUploadDir();
  }
  
  public function getWebPath()
  {
    return $this->getUploadDir() . '/' . $this->getId() . '.' . $this->getExtension();
  }
  
  public function __toString()
  {
    $imageSrc = $this->getUploadDir() . '/' . $this->getId() . '.' . $this->getExtension();
    
    $imageTag = '<img src="' . $imageSrc . '" alt="' . $this->getAlt() . '">'; 
    
    return $imageSrc;
  } 
  
  public function getFormatImage()
  {
    $imageSrc = '/' . $this->getUploadDir() . '/' . $this->getId() . '.' . $this->getExtension();
    
    
    return $imageSrc;
  }   

    public function __construct()
    {
	$this->dateCreation = new \DateTime();
    }  

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Image
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }
}
