<?php

namespace IP\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IP\CoreBundle\Entity\Tuto;
use IP\CoreBundle\Entity\Categorie;

/**
 * @Route("/")
 */
class DefaultController extends Controller
{ 
    /**
     * Home Page
     *
     * @Route("/")
     * @Method("GET")
     * @Template()
     */    
    public function welcomeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPCoreBundle:Tuto')->findAll();
        $categories = $em->getRepository('IPCoreBundle:Categorie')->findAll();

        return array(
            'entities' => $entities,
            'categories' => $categories
        );
    }

}
