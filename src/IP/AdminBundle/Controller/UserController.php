<?php

namespace IP\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use IP\UserBundle\Entity\User;
use IP\UserBundle\Form\UserType;
use IP\UserBundle\Form\Type\RegistrationFormType;


/**
 * @Route("/admin/user")
 */
class UserController extends Controller
{

    /**
     * @Route("/")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }        
        
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPUserBundle:User')->findAll();
        
        $deleteForm = $this->createDeleteForm();

        return array(
            'entities' => $entities,
            'deleteForm' => $deleteForm->createView(),
        );
    }
    
    /**
     * @Route("/{id}/edit", requirements={"id" = "\d+"})
     * @Method({"GET", "PUT"})
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }        
        
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $form = $this->createForm(new RegistrationFormType(), $entity, array('method' => 'PUT'));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array('class' => 'btn btn-primary')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', "L'utilisateur a été modifié avec succès.");
            return $this->redirect($this->generateUrl('ip_admin_user_index'));
        }        

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        );
    }    
    
    /**
     * @Route("/delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }        
        
        $form = $this->createDeleteForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $deleteFormRequest = $request->request->get('form');
            if (isset($deleteFormRequest['tags'])) {
                $entities2Delete = $deleteFormRequest['tags'];
                foreach ($entities2Delete as $key => $entity) {
                    $em = $this->getDoctrine()->getManager();
                    $entity = $em->getRepository('IPUserBundle:User')->find($key);
                    if (!$entity) {
                        throw $this->createNotFoundException('Unable to find entity.');
                    }
                    $em->remove($entity);                
                }   
                $em->flush();
            }
        }        

        return $this->redirect($this->generateUrl('ip_admin_user_index'));
    }    
    
    private function createDeleteForm()
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ip_admin_user_delete'))    
            ->setMethod('DELETE')
            ->add('tags', 'collection', array(
                'type' => 'hidden',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ))             
            ->add('submit', 'submit', array(
                'label' => 'Supprimer',
                'attr' => array('class' => 'hide'),
                ))
            ->getForm()
        ; 
    }    
    
    
    /**
     * @Route("/token/generate")
     * @Method("GET")
     * @Template()
     */      
    public function generateTokenAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }   
        
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $entity = 'not null';
            
            $cm = $this->container->get('ip_core.common');

            while ($entity !== null) {
                $generatedtoken = $cm->generateToken(8, "123456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ");
                
                $entity = $em->getRepository('IPUserBundle:User')->findOneByTokken($generatedtoken);                
            }
            
            return new Response($generatedtoken); 
        }        
    }    
    
    /* 
     * ///////////////////////
     * ---- Méthodes User ----
     * ///////////////////////
     */
    
    
    
    /* 
     * ///////////////////////
     * ---- Méthodes Communes ----
     * ///////////////////////
     */    
    
    /**
     * @Route("/edit")
     * @Method({"GET", "PUT"})
     * @Template()
     */
    // Mon profil
    public function userEditAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getUser();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }
        
//        if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
//            $formType = new RegistrationFormType();
//        } else {
//            $formType = new UserType();
//        }       
        
        $formType = new UserType();

        $form = $this->createForm($formType, $entity, array('method' => 'PUT'));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array('class' => 'btn btn-primary')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', "L'utilisateur a été modifié avec succès.");
        }        

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        );
    }    
}
