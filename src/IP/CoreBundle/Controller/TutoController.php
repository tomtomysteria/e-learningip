<?php

namespace IP\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use IP\CoreBundle\Entity\Tuto;
use IP\CoreBundle\Entity\Note;
use IP\CoreBundle\Form\TutoType;

/**
 * @Route("/tuto")
 */
class TutoController extends Controller
{

    /**
     * @Route("/")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPCoreBundle:Tuto')->findAll();

        $deleteForm = $this->createDeleteForm();

        return array(
            'entities' => $entities,
            'deleteForm' => $deleteForm->createView(),
        );
    }

    /**
     * @Route("/new")
     * @Method({"GET", "POST"})
     * @Template()
     */
    public function newAction(Request $request)
    {

        $entity = new Tuto();
        $form = $this->createForm(new TutoType(), $entity, array('method' => 'POST'));

        $form->add('submit', 'submit', array('label' => 'Enregistrer', 'attr' => array('class' => 'btn btn-primary')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            
            $tagsEntityFlushed = $entity->getTags();
            
            foreach ($tagsEntityFlushed as $tagEntity) {
                $tagEntity->setTuto($entity);
            }  
            
            $em->persist($entity);
          
          
            
            $em->flush();
            

            
            

            $request->getSession()->getFlashBag()->add('success', "Le type a été créé avec succès.");
            return $this->redirect($this->generateUrl('ip_core_default_welcome'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/{id}/edit", requirements={"id" = "\d+"})
     * @Method({"GET", "PUT"})
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPCoreBundle:Tuto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $form = $this->createForm(new TypeType(), $entity, array('method' => 'PUT'));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour', 'attr' => array('class' => 'btn btn-primary')));

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', "Le tuto a été modifié avec succès.");
            return $this->redirect($this->generateUrl('ip_core_tuto_index'));
        }

        return array(
            'entity'      => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * @Route("/{id}", requirements={"id" = "\d+"})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
      
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPCoreBundle:Tuto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        return array(
            'entity'      => $entity,
        );
    }

    /**
     * @Route("/delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedException();
        }

        $form = $this->createDeleteForm();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $deleteFormRequest = $request->request->get('form');
            if (isset($deleteFormRequest['tags'])) {
                $entities2Delete = $deleteFormRequest['tags'];
                foreach ($entities2Delete as $key => $entity) {
                    $em = $this->getDoctrine()->getManager();
                    $entity = $em->getRepository('IPCoreBundle:Tuto')->find($key);
                    if (!$entity) {
                        throw $this->createNotFoundException('Unable to find entity.');
                    }
                    $em->remove($entity);
                }
                $em->flush();
            }
        }

        return $this->redirect($this->generateUrl('ip_core_tuto_index'));
    }

    private function createDeleteForm()
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ip_core_tuto_delete'))
            ->setMethod('DELETE')
            ->add('tags', 'collection', array(
                'type' => 'hidden',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'label' => false,
            ))
            ->add('submit', 'submit', array(
                'label' => 'Supprimer',
                'attr' => array('class' => 'hide'),
            ))
            ->getForm()
            ;
    }

    /**
     * @Route("/note")
     * @Method("POST")
     */
    public function noteAction(Request $request)
    {
        // recuperer utilisateur
        $user = $this->getUser();

        if (!$user) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $tuto = $em->getRepository('IPCoreBundle:Tuto')->find($id);

        if (!$tuto) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        // modifier la note
        $note = new Note();

        $note->setTuto($tuto);
        $note->setUser($user);
        $note->setNote(true);

        $em->persist($note);
        
        $em->flush();

        $repository = $em->getRepository('IPCoreBundle:Note');
        $nbNotes = $repository->createQueryBuilder('p')
            ->where('p.tuto = :id')
            ->setParameter('id', $id)
            ->getQuery();

        $data = array('status' => "ok", "nbNotes" => count($nbNotes->getResult()));
        return new Response(json_encode($data));
    } 

}
