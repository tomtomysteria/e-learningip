<?php

// src/IP/CoreBundle/Twig/CountryExtension.php

namespace IP\CoreBundle\Twig;

class LanguageExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('language', array($this, 'languageFilter')),
        );
    }

    public function languageFilter($languageCode, $locale = "fr")
    {
        $c = \Symfony\Component\Locale\Locale::getDisplayLanguages($locale);
        
        return array_key_exists($languageCode, $c) ? $c[$languageCode] : $languageCode;
    }

    public function getName()
    {
        return 'language_extension';
    }

}
